# eagon &#x2014; Tool to encrypt a file using a passphrase

I used this project to learn more about *goroutines* and wet my foots on cryptography. Don't use this to REALY encrypt some files of yours, please.


## Usage

```yaml
description: Golang tool to encrypt and decrypt a file using a passphrase

command: eagon -f FILE [-e] [-o OUT_FILE]
examples:
- eagon -f secret.txt -e -o home_work.txt #to encrypt that file
- eagon -f home_work.txt -o secret.txt #to decript that file

options:
- --file|-f FILE: File to encrypt or decrypt
- --output|-o OUT_FILE: File to store the output of the decrypt and encrypt instruction (default is the same file name of the input)
- --encrypt|-e: Mark the file to encrypt, when empty, it will decrypt the file specified
```


# Todos

+ [ ] Instalation script