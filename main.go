package main

import (
    "github.com/spf13/pflag"
    "io/ioutil"
    "io"

    "crypto/sha256"
    "crypto/aes"
    "crypto/cipher"
    "crypto/rand"

    "time"
    "fmt"
)


const TIMEOUT_LENGTH = 20

var inputFileName string
var outputFileName string
var encryptOption bool

var fileContent []byte
var key []byte


//  WARNING: Read and store the command arguments before store them in each variable

func init() {
    pflag.BoolVarP(&encryptOption, "encript", "e", false,
        "Option to encript a file")

    pflag.StringVarP(&inputFileName, "file", "f", "",
        "File to encript/decrytp")

    pflag.StringVarP(&outputFileName, "output", "o", inputFileName,
        "File to store the result of the encription/decription")

    pflag.Parse()
}


//  MAIN: Read the password -> Save the input file content -> Encrypt or decrypt that input file

func main() {
    setupPasswordOptions()

    file, err := ioutil.ReadFile(inputFileName)
    errorHandler(err)
    fileContent = file

    if encryptOption {
        encryptFile()
    } else {
        decryptFile()
    }
}


//  INFO: Rad the user's password and save the SHA256 output of that string globaly

func setupPasswordOptions() {
    ch1 := make(chan string)
    ch2 := make(chan string)

    go func() {
        defer close(ch1)
        var passTMP string

        fmt.Print("Password: ")
        fmt.Scanf("%s", &passTMP)
        ch1 <- passTMP
    }()

    go func() {
        defer close(ch2)
        time.Sleep(time.Second * TIMEOUT_LENGTH)
        ch2 <- "Timeout..."
    }()

    select {
        case result := <-ch1:
            hash := sha256.Sum256([]byte(result))
            key = hash[:]            
        case result := <-ch2:
            panic(result)
    }
}


//  INFO: Setup the encrypt functions (???) and write the ENCRYPTED message in output file 

func encryptFile() {
    cphr, err := aes.NewCipher(key)
    errorHandler(err)

    gcm, err := cipher.NewGCM(cphr)
    errorHandler(err)

    nonce := make([]byte, gcm.NonceSize())
    _, err = io.ReadFull(rand.Reader, nonce)
    errorHandler(err)

    plainText := gcm.Seal(nonce, nonce, fileContent, nil)
    err = ioutil.WriteFile(outputFileName, plainText, 0777)
    errorHandler(err)

    fmt.Println(string(plainText))
}


//  INFO: Setup the encrypt functions (???) and write the DECRYPTED message in output file 

func decryptFile() {
    cipherText, err := ioutil.ReadFile(inputFileName)
    errorHandler(err)

    cphr, err := aes.NewCipher(key)
    errorHandler(err)

    gcm, err := cipher.NewGCM(cphr)
    errorHandler(err)

    nonceSize := gcm.NonceSize()
    if len(cipherText) < nonceSize {
        panic("The encryped file need to have a size of 32 byte...")
    }

    nonce := cipherText[:nonceSize]
    encryptedMessage := cipherText[nonceSize:]

    plainText, err := gcm.Open(nil, nonce, encryptedMessage, nil)
    err = ioutil.WriteFile(outputFileName, plainText, 0777)
    errorHandler(err)

    fmt.Println(plainText)
}


//  INFO: A better way to check the error without messing the code with if's statements... ;)

func errorHandler(err error) {
    if err != nil {
        panic(err)
    }
}
